[![build status](https://gitlab.com/e-om/php/badges/master/build.svg)](https://gitlab.com/e-om/php/commits/master) [![coverage report](https://gitlab.com/e-om/php/badges/master/coverage.svg)](https://gitlab.com/e-om/php/commits/master)

## Ejemplo de Test con PHP
Esta versión tiene un ejemplo de **CI** con **(PHP 5.6 + MySQL + PHPUnit + xDebug)** "Coverage Code" de como utilizar los pipelinos en GitLab y BitBucket,.
Los archivos de configuración son estos dos archivos.

**GitLab:** .gitlab-ci.yml

**BitBucket:** bitbucket-pipelines.yml

### Lo bueno de GitLab que podemos tener estos en los reportes ;)

[![build status](https://gitlab.com/e-om/php/badges/master/build.svg)](https://gitlab.com/e-om/php/commits/master)
[![coverage report](https://gitlab.com/e-om/php/badges/master/coverage.svg)](https://gitlab.com/e-om/php/commits/master)


# Proyecto de Ejemplo de como utilizar Pipelines en BitBucket con (PHP + MySQL + xDebug + PHPUnit) #


Todo se configura de la maquina Docker para hacer el test con el Pipeline se haces de **bitbucket-pipelines.yml** en el raiz del proyecto.


### **Conexión a MySQL desde otra imagen Docker dentro del Pipeline** ###

**Nombre de host:** *127.0.0.1* (evitar el uso localhost, ya que algunos clientes intentarán conectarse a través de un "socket de Unix" local, que no va a funcionar en tuberías)

**Puerto:** *3306*

**DB Predeterminada:** *hello_world_test*

**Usuario:** *test_user*, contraseña *test_user_password*. 

(El usuario root de MySQL no será accesible.)

Usted tendrá que llenar la base de tuberías con sus tablas y esquemas. Si necesita configurar el motor de base de datos subyacente más, consulte el funcionario imagen acoplable Hub  para más detalles.